# i3wm-dmenu-patch

Fontes usadas pra montar o patch

- https://i3wm.org/

- https://github.com/Airblader/i3

- https://github.com/ashinkarov/i3-extras

- http://tools.suckless.org/dmenu/patches/


# Screenshot
![i3wm-dmenu-patch](https://gitlab.com/zezaocapoeira/i3wm-dmenu-patch/raw/master/screenshot/patch-i3wm.png)
![i3wm-dmenu-patch](https://gitlab.com/zezaocapoeira/i3wm-dmenu-patch/raw/master/screenshot/patch-dmenu.png)
![i3wm-dmenu-patch](https://gitlab.com/zezaocapoeira/i3wm-dmenu-patch/raw/master/screenshot/icone-na-barra-de-titulo.png)
